FROM magento/magento2devbox-web

RUN usermod -a -G www-data magento2
RUN usermod -a -G magento2 www-data

LABEL maintainer="roman.rudavskyi@plumrocket.com"
LABEL version="2.2.3"
LABEL description="Magento 2.2.3"

ENV MAGENTO_VERSION 2.2.3
ENV INSTALL_DIR /var/www/magento2
ENV COMPOSER_HOME /var/www/.composer

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer
COPY ./auth.json $COMPOSER_HOME

RUN chsh -s /bin/bash magento2

RUN cd /tmp && \
  curl https://codeload.github.com/magento/magento2/tar.gz/$MAGENTO_VERSION -o $MAGENTO_VERSION.tar.gz && \
  su magento2 -c "tar xvf $MAGENTO_VERSION.tar.gz" && \
  mv magento2-$MAGENTO_VERSION/* magento2-$MAGENTO_VERSION/.htaccess $INSTALL_DIR

RUN su magento2 -c "cd $INSTALL_DIR && composer install"

RUN cd $INSTALL_DIR && \
  find var vendor pub/static pub/media app/etc -type f -exec chmod u+w {} \; && \
  find var vendor pub/static pub/media app/etc -type d -exec chmod u+w {} \; && \
  chmod u+x bin/magento

#RUN cd $INSTALL_DIR && php bin/magento setup:install \
#  --base-url=http://0.0.0.0:5000/ \
#  --backend-frontname=cpanel1 \
#  --key=cpanel \
#  --db-host=db --db-name=magento223 \
#  --db-user=mage --db-password=mage123 \
#  --admin-firstname=Magento --admin-lastname=User --admin-email=user@example.com \
#  --admin-user=root --admin-password=root123 --language=en_US \
#  --currency=USD --timezone=America/Chicago --cleanup-database \
#  --sales-order-increment-prefix="ORD$" --session-save=db --use-rewrites=1