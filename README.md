## Step 1

### `docker-compose up`


## Step 2

  1. Connect via SSH to web container
  ### `sudo docker exec -i -t docker-magento2_web_1 bash`
  2. Navigate to magento root
  ### `cd /var/www/magento2`
  3. Install Magento
  ### `php bin/magento setup:install --base-url=http://0.0.0.0:5000 --backend-frontname=cpanel1 --key=cpanel --db-host=db --db-name=magento223 --db-user=mage --db-password=mage123 --admin-firstname=Magento --admin-lastname=User --admin-email=user@example.com --admin-user=root --admin-password=root123 --language=en_US --currency=USD --timezone=America/Chicago --cleanup-database --sales-order-increment-prefix="ORD$" --session-save=db --use-rewrites=1`
  4. Change permissions
  ### `chown -R magento2:magento2 var/ && chown -R magento2:magento2 generated/`